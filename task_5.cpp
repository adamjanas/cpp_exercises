/*
1. Z pliku o nazwie podanej przez u�ytkownika wczyta� wierszami dane rzeczywiste
(mog� by� ujemne, mog� by� bez kropki) do tablicy A[n][n] (n - sta�a). Je�li danych w
pliku b�dzie za ma�o, wolne miejsca w tablicy A powinny by� wype�nione warto�ci�
sta�� x.
2. Wydrukowa� tablic� wierszami ze sta�� liczb� p miejsc po kropce i sta��
szeroko�ci� d kolumn.
3. Znale�� najwi�ksz� liczb� na g��wnej przek�tnej (lub pierwsz� napotkan� z kilku
najwi�kszych) i wpisa� j� do ca�ego wiersza, kt�rym znajduje si� ta liczba.
4. Znale�� najmniejsz� liczb� pod g��wn� przek�tn� (lub pierwsz� napotkan� z kilku
najmniejszych) i wpisa� j� do ca�ej kolumny, w kt�rej znajduje si� ta liczba.
5. Ponownie wydrukowa� tablic� (w formacie jak w p. 2).
6. Na koniec ca�� tablic� zapisa� wierszami (w formacie jak w p. 2) do pliku o nazwie
"nowa.txt".
*/

#include <iostream>
#include <iostream>
#include <fstream>
#include <cstdlib>
#include<algorithm>
#include<string>
#include <iomanip>

using namespace std;

const int n=5;
const int x=3;
const int p=3;
int col_width=4;
float biggest=1.175494e-38;
int verse;
float smallest=1.175494e38;
int col;

int main()
{
    fstream plik;
    float A[n][n];
    plik.open("dane_do_A.txt", ios::in);

    if(plik.good()==false){
        cout << "nie mo�na otworzy� pliku";
        exit(0);
    }

      for (int i = 0; i < n; i++){
        for (int j = 0; j < n; j++){
            A[i][j] = x;
        }
        }


    for(int i = 0; i < n; i++){
        for(int j = 0; j < n; j++){
            plik >> A[i][j];
            }
            }

        //prints tab
          for (int i = 0; i < n; i++){
            for (int j = 0; j < n; j++){
            cout << setw(col_width) << setprecision(p) << A[i][j] << "\t";
            }
            cout << endl;
        }
	
	//najwieksza na glownej przekatnej
        for (int i = 0; i < n; i++){
            if(A[i][i] > biggest || A[i][i] == biggest){
                biggest = A[i][i];
                verse = i;
            }
        }

        for (int j = 0; j < n; j++){
            A[verse][j] = biggest;
            }

        cout << endl << endl;

        for (int i = 0; i < n; i++){
            for (int j = 0; j < n; j++){
            cout << setw(col_width) << setprecision(p) << A[i][j] << "\t";
            }
            cout << endl;
        }
	
	//najmniejsza liczba pod glowna przekatna

        for (int i = 0; i < n-1; i++){
            if(A[i+1][i] < smallest || A[i+1][i] == smallest){
                smallest = A[i+1][i];
                col = i;
            }
        }

            for (int i = 0; i < n; i++){
            A[i][col] = smallest;
            }


    cout << endl << endl;


        for (int i = 0; i < n; i++){
            for (int j = 0; j < n; j++){
            cout << setw(col_width) << setprecision(p) << A[i][j] << "\t";
            }
            cout << endl;
        }
	

	//zapis do nowego pliku
       fstream plik2;

        plik2.open("nowa.txt", ios::out);

    if(plik2.good()==false){
        cout << "plik nie istnieje";
        exit(0);
    }


        for (int i = 0; i < n; i++){
            for (int j = 0; j < n; j++){
            plik2 << A[i][j] << "\t";
            }
            plik2 << endl;
        }

    plik.close();

    plik2.close();


    return 0;
}
