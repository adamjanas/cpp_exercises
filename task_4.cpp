/*
a. Tablicę A[w][k] (w, k - stałe) wypełnić liczbami losowymi całkowitymi dwucyfrowymi
(dodatnimi lub ujemnymi).
b. Wydrukować tablicę wierszami z ustawioną szerokością wydruku, uzupełniając
każdy wiersz na końcu średnią wartością wszystkich liczb w tym wierszu,
wydrukowaną z dokładnością do 2 miejsc po kropce.
c. Przesunąć cyklicznie o 1 miejsce w prawo wiersze, w których ta średnia jest większa
co do modułu od stałej G, po czym ponownie wydrukować tablicę.
d. Dla każdej kolumny wydrukować (pod tą kolumną), ile jest w niej liczb podzielnych przez
stałą D.
e. Wydrukować indeksy kolumn, które mają najmniej liczb podzielnych przez stałą D.
*/

#include <iostream>
#include <time.h>
#include <iomanip>
#include <time.h>
#include <cmath>

using namespace std;

int w=5;
int k=6;
int sum=0;

int main()
{

    int A[w][k];

    for(int i=0; i<w; i ++){
        for(int j=0; j<k; j ++){
            A[i][j] = (rand() % 90) + 10;
            if(rand() % 2 == 0){
                A[i][j] *= -1;
            }
        }
    }

    for(int i=0; i<w; i ++){
        for(int j=0; j<k ; j ++){
            cout << A[i][j] << "\t";
        }
        cout << endl;
        cout << endl;
    }

    for(int i=0; i<w; i++){
        for(int j=0; j<k; j++){
            cout <<  setw(4) << A[i][j] << "\t";

        }
        cout << endl;
        cout << endl;
    }

    cout << endl;

    cout << "okay, now lets add element average of values in accurately this verse at the end in every verse:  " << endl << endl;

    int sum=0;

    for(int i=0; i<w; i++){
        for(int j=0; j<k; j++){
            cout << A[i][j] << "\t";
            sum+=A[i][j];
        }
        cout << " | " << setprecision(2) << double(sum)/double(k) << "\t";
        sum=0;
        cout << endl;
        cout << endl;
    }

     cout << "now lets move our array 1 place towards right (periodically) where average of elements is bigger than constant G :  " << endl;

    const int G=8;

        for(int i=0; i<w; i++){
        for(int j=0; j<k; j++){
            sum+=A[i][j];
            if(abs(double(sum)/double(k)) > G){
            int schowek = A[i][k-1];
            for (int j = k-1; j > 0; j--)
            A[i][j] = A[i][j-1];
            A[i][0] = schowek;
            }

        }
        sum=0;
        cout << endl;
        cout << endl;
    }

        for(int i=0; i<w; i++){
        for(int j=0; j<k; j++){
            cout << A[i][j] << "\t";

        }
        cout << endl;
        cout << endl;
    }

    cout << "print how many digits is divided by constant D in columns: " << endl;

    const int D=3;
    int how_many=0;
    int lowest=INT_MAX;
    int column;
    int ileliczb[k];

    for(int j=0; j<k; j++){
        for(int i=0; i<w; i++){
            if(A[i][j] % D == 0){
                how_many++;
            }
            ileliczb[j] = how_many;
        }
        cout << j + 1 << " column" <<" | " << how_many << "\t";
        if(how_many < lowest){
            lowest = how_many;
        }
        how_many=0;
        cout << endl;
        cout << endl;
    }

    cout << "indexes of the columns with the smallest amount of digits divided by D: " << endl;

    for(int i=0; i<k; i++){
        if(lowest == ileliczb[i]){
           cout << "index: " << i << endl;
        }
    }
  return 0;
}
