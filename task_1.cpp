/*
Napisać program, który:
1. Pyta użytkownika o imię, wczytuje je i drukuje powitanie z wykorzystaniem tego
imienia.
2. Wczytuje dwie dowolne liczby całkowite do zmiennych typu int.
3. Drukuje ilość wczytanych liczb ujemnych.
4. Drukuje wynik dzielenia mniejszej liczby przez podwojoną wartość większej.
5. Drukuje informację, czy mniejsza z wczytanych liczb jest jednocyfrowa, czy
dwucyfrowa, czy też ma więcej cyfr.
*/

#include <iostream>

using namespace std;

string name, division_msg, digits_msg;

int number1, number2, negative_count = 0;

double division;

int main()
{
    cout << "Insert your name: ";
    cin >> name;

    cout << "Your name is " << name << endl;

    cout << "Enter first number: ";
    cin >> number1;

    cout << "Enter second number: ";
    cin >> number2;

    cout << "First number: " << number1 << endl << "Second number: " << number2 << endl;

    if (number1<0)
    {
        negative_count++;
    }
    if (number2 <0)
    {
        negative_count++;
    }

    cout << "Amount of negative numbers: " << negative_count << endl;

    if (number1>number2 && number1!=0)
    {
        division = double(number2)/(2*double(number1));
        cout << "Division equals: " << division << endl;
    }
    else if (number1<number2 && number2 != 0)
    {
        division = double(number1)/(2*double(number2));
        cout << "Division equals: " << division << endl;
    }
    else if (number1 == number2)
    {
        division_msg = "Numbers are equal, in that case we cant do our division. Try with different numbers.";
        cout << division_msg << endl;
    }
    else
    {
        division_msg = "We cant divide by 0!";
        cout << division_msg << endl;
    }

    if (number1 > number2)
    {
        if (number2<10 && number2>-10)
        {
            digits_msg = "Second number (lower one) is single-digit ";
            cout << digits_msg << endl;
        }
        else if (number2>10 && number2<100 || number2<-10 && number2>-100)
        {
            digits_msg = "Second number (lower one) is two-digit ";
            cout << digits_msg << endl;
        }
        else
        {
            digits_msg = "Second number (lower one) has more than two digits ";
            cout << digits_msg << endl;
        }
    }
    else if (number2 > number1)
    {
         if (number1<10 && number1>-10)
        {
            digits_msg = "First number (lower one) is single-digit ";
            cout << digits_msg << endl;
        }
        else if (number1>10 && number1<100 || number1<-10 && number1>-100)
        {
            digits_msg = "First number (lower one) is two-digit ";
            cout << digits_msg << endl;
        }
        else
        {
            digits_msg = "First number (lower one) has more than two digits ";
            cout << digits_msg << endl;
        }
    }
    else
    {
            digits_msg = "Numbers are equal, so we cannot check amount of digits in lower one.";
            cout << digits_msg << endl;
    }
    return 0;
}
