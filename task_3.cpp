
/*
Napisać program, który:
a. wczytuje napisy aż do wczytania napisu kończącego się kropką i drukuje informację, o ile
najdłuższy wczytany napis jest dłuższy od najkrótszego. Nie należy brać pod uwagę ostatniego
napisu (tego z kropką) kończącego ciąg. Należy zastosować pętlę while.
b. potem wczytuje liczbę całkowitą k, wymuszając, by k było dodatnie i mniejsze od stałej g (pętlą
do-while).
c. następnie wczytuje ciąg k napisów (pętlą for), po czym drukuje łączną ilość znaków będących
cyframi w tych wszystkich napisach.
d. na koniec losuje n dużych liter (n – stała) do tablicy Z[n], drukuje tę tablicę, zamienia dwa
ostatnie znaki z dwoma pierwszymi i ponownie drukuje tablicę (to wszystko pętlą for).
*/

#include <iostream>
#include <string>
#include <typeinfo>
#include <time.h>
#include <algorithm>
using namespace std;

string label;
int shortest = INT_MAX;
int longest = 0;

int xd;
int k=0;
int const g = 11;

string some_text;
int count_digits;

int const n = 7;
string Z[n];
string letters = "ABCDEFGHIJKMNOPRSTUWZ";
string x,y;


int main()
{
    // first task

    while(label.back() != '.'){
    cout << "Enter a string: ";
    cin >> label;
    if (label.back() == '.') { break; }
    if (label.size() < shortest) { shortest = label.size(); }
    if (label.size() > longest) { longest = label.size(); }
    }

    cout << "Difference in length between longest and shortest string equals: " << longest - shortest << "\n";

    // second task

    do
    {
        cout << "Enter positive integer smaller than our constant G: ";
        cin >> xd;
        if (xd > 0 && xd < g){
            k = xd;
        } else{
            cout << "Our integer should be positive and smaller than G, try again." << "\n";
        }
    }while(xd <= 0 || xd >= g);

    cout << "Okay our variable k equals: " << k << "\n";

    //third task

    for(int i=0; i<k; i++){
        cout << "Enter " << i+1 << " string, (we gonna count digits in them): " << "\n";
        cin >> some_text;
        for(int i=0; i<some_text.size(); i++){
            if(isdigit(some_text[i])){
                    count_digits++;
            }
        }
    }

    cout << "In our strings were: " << count_digits << " digit/digits" << "\n";

    // fourth task

    srand(time(NULL));

    for(int i=0; i<n; i++){
        Z[i] = letters[rand()%letters.size()-1];
        cout << Z[i] << "\n";
    }

    swap(Z[0], Z[n-1]);
    swap(Z[1], Z[n-2]);

    for(int i=0; i<n; i++){
        cout << i + 1 << " element: " << Z[i] << "\n";
    }

    return 0;
}
